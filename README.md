# README


* environment.js
  ```
  const { environment } = require('@rails/webpacker')

  const webpack = require('webpack')

  environment.plugins.prepend('Provide',
    new webpack.ProvidePlugin({
      $: 'jquery/src/jquery',
      jQuery: 'jquery/src/jquery'
    })
  )
  environment.loaders.append('jquery', { 
    test: require.resolve('jquery'), 
    use: [{ loader: 'expose-loader',
            options: '$', 
          }, 
          { loader: 'expose-loader', 
            options: 'jQuery', 
          }], 
  });

  module.exports = environment
 
  ```

* application.js
  
  ```
  require("@rails/ujs").start()
  require("turbolinks").start()
  require("@rails/activestorage").start()
  require("channels")

  require("jquery")


  require("select2/dist/js/select2.min.js")
  import "select2/dist/css/select2.css"

  ```

 * books _form.html.erb
    ```
    <%= form_with(model: book, local: true) do |form| %>
    <% if book.errors.any? %>
      <div id="error_explanation">
        <h2><%= pluralize(book.errors.count, "error") %> prohibited this book from being saved:</h2>

        <ul>
          <% book.errors.full_messages.each do |message| %>
            <li><%= message %></li>
          <% end %>
        </ul>
      </div>
    <% end %>

    <div class="field">
      <%= form.label :title %>
      <%= form.text_field :title %>
    </div>

    <div class="field">
      <%= form.label :author %>
      <%= form.text_field :author %>
    </div>

    <div class="field">
      <%= form.label :category %>
      <%= collection_select :book, :category_id, Category.all, :id, :name%>
    </div>

    <div class="actions">
      <%= form.submit %>
    </div>
      <% end %>
    
      <script>
        $('select').select2();
      </script>
 
 